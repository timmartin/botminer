SOURCES = js/source/robots.js
DELIVERABLES = \
	bundle.js \
	index.html \
	level2.html \
	css \
	img \
	codemirror

include config.mk

default: bundle.js

bundle.js: build
	browserify js/build/types.js js/build/robots.js js/build/gui.js js/build/levels.js -o bundle.js

build: $(SOURCES)
	babel --presets react,es2015 js/source -d js/build

deploy: bundle.js
	scp -r $(DELIVERABLES) $(UPLOAD_DEST)
