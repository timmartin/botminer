import {ResourceTypes} from './types.js';

function emptyResources(width, height) {
    var i, j;

    var result = [];

    for (i = 0; i < height; i++) {
	var row = [];
	for (j = 0; j < width; j++) {
	    row[j] = ResourceTypes.NOTHING;
	}

	result[i] = row;
    }

    return result;
}

var level1 = {
    settings: {
        width: 10,
        height: 10
    },

    initialBotPosition: {
        x: 0, y: 0
    },

    initialResources: function() {
        var result = emptyResources(this.settings.width, this.settings.height);

        result[3][4] = ResourceTypes.GOLD;
        result[8][2] = ResourceTypes.GOLD;
        return result;
    },

    targetReached: function(worldState) {
        return (worldState.resourcesGathered.GOLD >= 2);
    }
};

var level2 = {
    settings: {
        width: 10,
        height: 10
    },

    initialBotPosition: {
        x: 0, y: 0
    },

    initialResources: function() {
        var result = emptyResources(this.settings.width, this.settings.height);

        result[9][0] = ResourceTypes.GOLD;
        result[9][9] = ResourceTypes.GOLD;
        result[7][3] = ResourceTypes.GOLD;
        result[6][6] = ResourceTypes.GOLD;
        result[8][2] = ResourceTypes.GOLD;
        result[5][5] = ResourceTypes.GOLD;
        result[3][3] = ResourceTypes.GOLD;
        return result;
    },

    targetReached: function() {
        return false;
    }
};

export var currentLevel = level1;

export function nextLevel() {
    $.get("level2.html", function(data) {
        $("#introTextModal .modal-title").html(
            $(data).filter(".modal-title").html());
        $("#introTextModal .modal-body").html(
            $(data).filter(".modal-body").children());
        $("#introTextModal").modal();
    }, "html");
    currentLevel = level2;
    localStorage.setItem("current_level", 2);
}

/**
 * Run on startup to set the current level based on the information
 * about current level stored in local storage.
 */
export function restoreCurrentLevel() {
    var currentLevelNum = localStorage.getItem("current_level") || 1;

    if (currentLevelNum == 1) {
        currentLevel = level1;
        $("#introTextModal").modal();
    }
    else if (currentLevelNum == 2) {
        nextLevel();
    }
}
