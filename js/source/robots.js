'use strict';

import {ResourceTypes} from './types.js';
import * as levels from './levels.js';

var bind = function(func, thisValue) {
    return function() {
        return func.apply(thisValue, arguments);
    }
};

export var settings = levels.currentLevel.settings;

export var worldState = {
    botPosition: Object.assign({}, levels.currentLevel.initialBotPosition),

    listeners: [],

    resources: levels.currentLevel.initialResources(),

    resourcesGathered: {
        GOLD: 0
    },

    reset: function() {
        this.botPosition = Object.assign({},
                                         levels.currentLevel.initialBotPosition);
        this.resources = levels.currentLevel.initialResources();
        this.resourcesGathered = {GOLD: 0};
        this._updateListeners();
    },

    up: function() {
        if (this.botPosition.y > 0) {
            this.botPosition.y -= 1;
        }
        this._updateListeners();
    },

    down: function() {
        if (this.botPosition.y < settings.height - 1) {
            this.botPosition.y += 1;
        }
        this._updateListeners();
    },

    left: function() {
        if (this.botPosition.x > 0) {
            this.botPosition.x -= 1;
        }
        this._updateListeners();
    },

    right: function() {
        if (this.botPosition.x < settings.width - 1) {
            this.botPosition.x += 1;
        }
        this._updateListeners();
    },

    _updateListeners: function() {
        this.listeners.forEach(function(listener) {
            listener.setState({world: worldState});
        });
    }
};

var scriptEditor;

function callAndRepeat(howMany, action) {
    action();
    howMany--;
    if (howMany > 0) {
	return function() {
	    return callAndRepeat(howMany, action);
	};
    }
}

var instructionSet = {
    up: function(state) {
        var num = state.stack.pop()
	return callAndRepeat(num, bind(worldState.up, worldState));
    },

    down: function(state) {
        var num = state.stack.pop();
	return callAndRepeat(num, bind(worldState.down, worldState));
    },

    left: function(state) {
        var num = state.stack.pop();
        return callAndRepeat(num, bind(worldState.left, worldState));
    },

    right: function(state) {
        var num = state.stack.pop();
        return callAndRepeat(num, bind(worldState.right, worldState));
    },

    push: function(num) {
        return function(state) {
            state.stack.push(num);
        }
    },

    "?do": function(state) {
        state.loopStack.push({
            returnCounter: state.programCounter,
            remaining: state.stack.pop()
        });
    },

    loop: function(state) {
        var stackElement = state.loopStack[state.loopStack.length - 1];
        if (stackElement.remaining > 0) {
            stackElement.remaining--;
        }

        if (stackElement.remaining > 0) {
            state.programCounter = stackElement.returnCounter;
        }
        else {
            state.loopStack.pop();
        }
    }
};

export var executionState = {
    program: [],

    programCounter: 0,

    /**
     * If the current action didn't finish after one tick, it will
     * yield a closure that will be stored here, which should be
     * executed on the next tick repeatedly until it returns
     * undefined.
     */
    pendingActionClosure: undefined,

    stack: [],

    listeners: [],

    loopStack: [],

    running: false,

    codeSize: undefined,

    reset: function() {
        this.cancelTimer();
        this.stack = [];
        this.loopStack = [];
        this.recalculateCodeSize();
        this._updateListeners();
    },

    singleStep: function() {
        if (this.pendingActionClosure) {
            this.pendingActionClosure = this.pendingActionClosure();
            if (!this.pendingActionClosure) {
		this.programCounter++;
	    }
            this._updateListeners();
            return true;
	}
        else if (this.programCounter < this.program.length) {
            this.pendingActionClosure = this.program[this.programCounter](this);
	    if (!this.pendingActionClosure) {
		this.programCounter++;
	    }
            this._updateListeners();
            return true;
        }
        else {
            return false;
        }
    },

    timerPop: function() {
        var remaining = this.singleStep();

        if (remaining) {
            this.setTimer();
        }
    },

    setTimer: function() {
        this.timer = setTimeout(bind(this.timerPop, this), 600);
    },

    cancelTimer: function() {
	clearTimeout(this.timer);
    },

    loadScript: function() {
        this.running = true;
        localStorage.setItem("script", scriptEditor.getDoc().getValue());

        worldState.reset();
        this.program = [];
        this.stack = [];
        this.pendingActionClosure = undefined;
        var script = scriptEditor.getDoc().getValue(" ").split(/\s+/);
        var localExecutionState = this;
        script.forEach(function(token) {
            localExecutionState.program.push(
                localExecutionState._stringToToken(token));
        });
        this.programCounter = 0;
    },

    /**
     * Recalculate the number of instructions in the code. This is
     * used by some levels where there is a limit on the code size.
     */
    recalculateCodeSize: function() {
        var script = scriptEditor.getDoc().getValue(" ").trim().split(/\s+/);
        this.codeSize = script.length;
        this._updateListeners();
    },

    _updateListeners: function() {
        var localState = this;
        this.listeners.forEach(function (listener) {
            listener(localState);
        });
    },

    _stringToToken: function(tokenString) {
        if (tokenString.match(/^\d+$/)) {
            return instructionSet.push(parseInt(tokenString));
        }
        else if (instructionSet.hasOwnProperty(tokenString)) {
            return instructionSet[tokenString];
        }
    }
};

/**
 * Listener that deletes resources and updates the score information
 * when the robot has moved.
 */
worldState.listeners.push({
    setState: function(state) {
        var resources = state.world.resources;
        var botPosition = state.world.botPosition;
        var resourceAtPosition = resources[botPosition.y][botPosition.x];

        if (resourceAtPosition == ResourceTypes.GOLD) {
            state.world.resourcesGathered.GOLD++;
            resources[botPosition.y][botPosition.x] = ResourceTypes.NOTHING;
        }

        if (levels.currentLevel.targetReached(state.world)) {
            levels.nextLevel();
            executionState.reset();
            worldState.reset();
        }
    }
});

$(function() {
    scriptEditor = CodeMirror(
        document.getElementById("editor"),
        {value: localStorage.getItem("script") || ""});

    scriptEditor.on("changes", function(editor, changes) {
        executionState.recalculateCodeSize();
    });

    levels.restoreCurrentLevel();
    executionState.reset();
    worldState.reset();

    $("#introTextLink").click(function() {
        $("#introTextModal").modal("show");
    });

    $("#aboutLink").click(function() {
        $("#aboutModal").modal("show");
    });
});
