import React from 'react';
import ReactDOM from 'react-dom';

import {ResourceTypes} from './types.js';
import {settings, worldState, executionState} from './robots.js';

var PlayGrid = React.createClass({
    propTypes: {
        width: React.PropTypes.number.isRequired,
        height: React.PropTypes.number.isRequired
    },

    getInitialState: function() {
        return {
            world: worldState
        };
    },

    render: function() {
        var rows = [];
        var row_idx;
        for (row_idx = 0; row_idx < this.props.height; row_idx++) {
            var columns = [];
            var col_idx;
            for (col_idx = 0; col_idx < this.props.width; col_idx++) {
                var display = this.getDisplay(col_idx, row_idx);

                columns.push(<td key={col_idx}>{display}</td>);
            }

            rows.push(<tr key={row_idx}>{columns}</tr>);
        }
        return (
            <table id="play_grid_table">
            <tbody>
            {rows}
            </tbody>
            </table>
        );
    },

    /**
     * Get the display value for a specific cell based on the current
     * state of the grid.
     */
    getDisplay: function(x, y) {
        if (this.state.world.botPosition.x == x
            && this.state.world.botPosition.y == y) {
            return <img src="img/robot_icon.svg" className="play_grid_icon"></img>;
        }
        else {
            var resource = this.state.world.resources[y][x];
            if (resource == ResourceTypes.GOLD) {
                return "gold";
            }
            else {
                return "-";
            }
        }
    }
});

var Controls = React.createClass({
    propTypes : {
        run: React.PropTypes.func.isRequired,
        pause: React.PropTypes.func.isRequired,
        step: React.PropTypes.func.isRequired
    },

    render: function() {
        return <div className="btn-group">
            <button className="btn btn-default" onClick={this.props.run}>
            <span className="glyphicon glyphicon-play"></span>
            </button>
	    <button className="btn btn-default" onClick={this.props.pause}>
	    <span className="glyphicon glyphicon-pause"></span>
	</button>
            <button className="btn btn-default" onClick={this.props.step}>
            <span className="glyphicon glyphicon-step-forward"></span>
            </button>
        </div>;
    }
});

var StackView = React.createClass({
    getInitialState: function() {
        return {stack: []};
    },

    render: function() {
        var rows = this.state.stack.map(function (element, index) {
            return <div key={index}>{element}</div>;
        });

        return <div>{rows}</div>;
    }
});

var ScoreView = React.createClass({
    getInitialState: function() {
        return {
            resourcesGathered: {
                GOLD: 0
            }
        };
    },

    render: function() {
        return <table className="table">
            <tbody>
            <tr>
            <td>Gold gathered</td><td>{this.state.resourcesGathered.GOLD}</td>
            </tr>
            </tbody>
        </table>;
    }
});

var ProgramSizeView = React.createClass({
    getInitialState: function() {
        return {programSize: 0};
    },

    render: function() {
        var sizeLimit = this.state.limit || "unlimited";

        return <p>{this.state.programSize} / {sizeLimit}</p>
    }
});

var grid = ReactDOM.render(
    React.createElement(PlayGrid, {
        width: settings.width,
        height: settings.height
    }),
    document.getElementById("play_grid")
);

worldState.listeners.push(grid);

ReactDOM.render(
    React.createElement(Controls, {
        run: function() {
            executionState.loadScript();
            executionState.setTimer();
        },
        pause: function() {
            executionState.cancelTimer();
        },
        step: function() {
            if (!executionState.running) {
                executionState.loadScript();
            }
            executionState.singleStep();
        }
    }),
    document.getElementById("controls")
);

var stackView = ReactDOM.render(
    React.createElement(StackView),
    document.getElementById("stack_view")
);

var programSizeView = ReactDOM.render(
    React.createElement(ProgramSizeView),
    document.getElementById("program_size_view")
);

executionState.listeners.push(function(executionState) {
    stackView.setState({stack: executionState.stack});
    programSizeView.setState({programSize: executionState.codeSize});
});

var scoreView = ReactDOM.render(
    React.createElement(ScoreView),
    document.getElementById("score_view")
);

/**
 * Update the score based on resources gathered.
 *
 * TODO There's a hidden dependency on listener order here, because
 * the code that updates the resourcesGathered is also a listener in
 * worldState.listeners.
 */
worldState.listeners.push({
    setState: function(state) {
        scoreView.setState({
            resourcesGathered: state.world.resourcesGathered
        });
    }
});
